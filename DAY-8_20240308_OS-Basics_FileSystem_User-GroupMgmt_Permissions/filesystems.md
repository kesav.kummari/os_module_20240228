
bash-5.2# pwd
/root

bash-5.2# ls -lrt /home
drwxrwxrwx. 4 cloudshell-user cloudshell-user 4096 Mar  8 04:21 cloudshell-user

bash-5.2# cat /etc/passwd

USERNAME:X: UID:    GID:    Description :HomeDirectory  :ShellEnvironment
root    :x: 0  :      0:    root        :/root          :/bin/bash

bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/usr/sbin/nologin
systemd-oom:x:998:998:systemd Userspace OOM Killer:/:/usr/sbin/nologin
systemd-resolve:x:193:193:systemd Resolver:/:/usr/sbin/nologin

Normal user:
cloudshell-user:x:1000:996::/home/cloudshell-user:/bin/bash

nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin

Min : 0
Max : 65534