# Boot Process & Run levels

#### Here's a table comparing the boot process of Windows, Linux, and Unix:

|Process Step | Windows	| Linux	| Unix |
|-------------|---------|-------|-----|
| 1. BIOS/UEFI Firmware	| The BIOS/UEFI firmware initializes hardware devices and checks for system integrity. | The BIOS/UEFI firmware initializes hardware devices and checks for system integrity. | The BIOS initializes hardware devices and checks for system integrity. |
| 2. Bootloader	| The bootloader is responsible for loading the Windows kernel and other system files.	| The bootloader (GRUB, LILO, etc.) loads the Linux kernel and initial RAM disk (initrd). | The bootloader (GRUB, LILO, etc.) loads the Unix kernel and initial RAM disk (initrd). |
| 3. Kernel	| The Windows kernel is loaded and initializes device drivers and system services.	| The Linux kernel is loaded and initializes device drivers and system services. | The Unix kernel is loaded and initializes device drivers and system services. |
| 4. Init System | Windows does not have a separate init system. | Linux uses a system called init or systemd to start system services and daemons.	| Unix uses a system called init or systemd to start system services and daemons. |
| 5. Runlevel/Target | Windows does not use runlevels or targets. | Linux uses runlevels (0-6) or systemd targets to define system states and specify which services to start or stop.|Unix uses runlevels (0-6) or systemd targets to define system states and specify which services to start or stop. |
| 6. Services | Windows services are started by the Service Control Manager (SCM) and can be configured to start automatically or manually. | Linux services are started by the init system or systemd and can be configured to start automatically or manually. | Unix services are started by the init system or systemd and can be configured to start automatically or manually. |
| 7. Graphical User Interface (GUI)	| The Windows GUI is started by the Windows Explorer shell, which is loaded as a user-level process. | The Linux GUI (X Window System) is started as a user-level process and can be customized with different desktop environments (GNOME, KDE, etc.).	| The Unix GUI (X Window System) is started as a user-level process and can be customized with different desktop environments (GNOME, KDE, etc.). |

#### Note: 
    - This table provides a general overview of the boot process for Windows, Linux, and Unix. 
    - The exact details and steps may vary depending on the specific operating system and configuration.

# Windows Linux and Unix Run Levels table

|Run Level | Windows	| Linux | Unix |
|-------------|---------|-------|-----|
| 0 |	System halt/shutdown | System halt/shutdown | System halt/shutdown |
| 1 |	Single-user mode | Single-user mode | Single-user mode |
| 2 |	Not used | User-defined | User-defined |
| 3 |	Multi-user mode with networking	 | Multi-user mode with networking | Multi-user mode with networking |
| 4 |	Not used  |User-defined  | User-defined |
| 5 |	Multi-user mode with networking and GUI | Multi-user mode with networking and GUI	| Multi-user mode with networking and GUI |
| 6 |	System reboot | System reboot | System reboot |


#### Note: 
    
    - Windows does not have traditional runlevels like Linux and Unix. 
    
    - Instead, it has different system states, such as Safe Mode and Normal Mode, which are activated during the boot process. 
    
    - Linux and Unix runlevels can vary depending on the distribution and configuration. 
    
    - Some Linux distributions use systemd targets instead of runlevels. 
    
    - The exact functionality of each runlevel can also vary depending on the specific operating system and configuration.


# Text Editors in Linux 

####
    1. Insert Mode: 
        - i = inserts the text at current cursor position
        - I = inserts the text at beginning of line
        - A = Appends the text at end of line
        - o = inserts a line below current cursor position
        - O = inserts a line above current cursor position
        - r = replace a single char at current cursor position

    2. Execute Mode:
        - :q       = quit without saving
        - :!       = forcefully
        - :q!	     = quit forcefully without saving
        - :w       = save
        - :wq      = save & quit
        - :wq!     = save, quit forcefully
        - :se nu   = setting line numbers for reading only 
        - :se nonu = Removing the line numbers
        - :20      = Press enter to go to specific line like 20
        - :6,10 w! <new_file> : We can copy desire lines     [ :12,18 w! >>/root/Desktop/mac.txt ] 
        - :%s/cursor/devops/g : Search and Replace 

    3. Command Mode:
        - dd   = Deletes a line 
        - ndd  = Delete 2 lines  # 2dd 
        - yy   = Copy a line
        - nyy  = copies 3 lines  # 3yy # 100yy
        - p    = put (paste deleted or copied text)
        - u    = undo (can undo 1000 times)
        - ctrl+r = Redo
        - G      = Moves cursor to last line of file
        - /<word to find> = To search for a particular word.