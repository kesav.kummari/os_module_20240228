
bash-5.2# pwd
/root

bash-5.2# ls -lrt /home
drwxrwxrwx. 4 cloudshell-user cloudshell-user 4096 Mar  8 04:21 cloudshell-user

bash-5.2# cat /etc/passwd

USERNAME:X: UID:    GID:    Description :HomeDirectory  :ShellEnvironment
root    :x: 0  :      0:    root        :/root          :/bin/bash

bin:x:1:1:bin:/bin:/sbin/nologin
daemon:x:2:2:daemon:/sbin:/sbin/nologin
adm:x:3:4:adm:/var/adm:/sbin/nologin
lp:x:4:7:lp:/var/spool/lpd:/sbin/nologin
sync:x:5:0:sync:/sbin:/bin/sync
shutdown:x:6:0:shutdown:/sbin:/sbin/shutdown
halt:x:7:0:halt:/sbin:/sbin/halt
mail:x:8:12:mail:/var/spool/mail:/sbin/nologin
operator:x:11:0:operator:/root:/sbin/nologin
games:x:12:100:games:/usr/games:/sbin/nologin
ftp:x:14:50:FTP User:/var/ftp:/sbin/nologin
dbus:x:81:81:System message bus:/:/sbin/nologin
systemd-network:x:192:192:systemd Network Management:/:/usr/sbin/nologin
systemd-oom:x:998:998:systemd Userspace OOM Killer:/:/usr/sbin/nologin
systemd-resolve:x:193:193:systemd Resolver:/:/usr/sbin/nologin

Normal user:
cloudshell-user:x:1000:996::/home/cloudshell-user:/bin/bash

nobody:x:65534:65534:Kernel Overflow User:/:/sbin/nologin

Min : 0
Max : 65534



#### User & Group Managment 

AmazonLinux/Redhat/CentOS/Fedora/Oracle Linux Distributions:


User DB Files:
    /etc/passwd
    /etc/shadow

Group DB Files:
    /etc/group 
    /etc/gshadow

Users Type:

1. System Users : 0 - 999

2. Normal Users : 1000 - 65534

Different Shells:
    1. sh 
    2. bash
    3. csh
    4. tcsh
    5. ksh
    6. zsh

1. Create a User Group:

$ sudo grep cloudops /etc/group /etc/gshadow

$ sudo groupadd cloudops

/etc/group:cloudops:x:1001:
/etc/gshadow:cloudops:!::

2. Add Users to a Group:

- Create a User along with Group :

$ sudo useradd kesav

[ec2-user@ip-172-31-0-30 ~]$ sudo grep kesav /etc/passwd /etc/shadow /etc/group /etc/gshadow

/etc/passwd:kesav:x:1001:1002::/home/kesav:/bin/bash
/etc/shadow:kesav:!!:19793:0:99999:7:::

/etc/group:kesav:x:1002:
/etc/gshadow:kesav:!::


sudo useradd joel -G 1001 

sudo grep joel /etc/passwd /etc/shadow /etc/group /etc/gshadow
/etc/passwd:joel:x:1002:1003::/home/joel:/bin/bash
/etc/shadow:joel:!!:19793:0:99999:7:::
/etc/group:cloudops:x:1001:joel
/etc/group:joel:x:1003:
/etc/gshadow:cloudops:!::joel
/etc/gshadow:joel:!::

sudo useradd tom -g 1001

sudo grep tom /etc/passwd /etc/shadow /etc/group /etc/gshadow
/etc/passwd:tom:x:1003:1001::/home/tom:/bin/bash
/etc/shadow:tom:!!:19793:0:99999:7:::



Debian/Ubuntu Linux Distributions:

$ sudo addgroup opscloud

